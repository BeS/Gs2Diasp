#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Gs2Diasp.py: based on feedDiasp from Moritz Duchêne and Alexey Veretennikov with some modifications to replace
#              GNU Social specific formatting
#
# Copyright (C) 2015  Bjoern Schiessle <bjoern@schiessle.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from feedDiasp.feedDiasp.FeedDiasp import FeedDiasp
from feedDiasp.feedDiasp.Diasp import Diasp
from GnuSocial import GnuSocial

class Gs2Diasp(FeedDiasp):

    def __init__(self, pod, username, password, db, parser, gsUserName, gsFullName, keywords=None, hashtags=None, append=None):
        FeedDiasp.__init__(self, pod, username, password, db, parser, keywords, hashtags, append)
        self.diasp = Diasp(pod=self.pod, username=self.username, password=self.password, provider_name="GNU social")
        self.gnusocial = GnuSocial(gsUserName, gsFullName)

    def publish(self):
        """ publish post to Diaspora """
        if not self.diasp.logged_in:
            self.diasp.login()
        self.feed.update()
        posts = self.feed.get_entries()
        for post in posts:
            cleanTitle = self.gnusocial.cleanPost(post['title'])
            cleanTitle = self.gnusocial.group2Hashtag(cleanTitle)
            if not self.db.is_published(post['id']) and self.gnusocial.shouldPost(cleanTitle):
                print 'Published: ' + cleanTitle
                hashtags = self.find_hashtags(cleanTitle, self.keywords)
                if self.hashtags is not None:
                    hashtags.extend(self.hashtags)
                try:
                    self.diasp.post(text=cleanTitle, title=None, hashtags=hashtags, source=None, append=self.append)
                    self.db.mark_as_posted(post['id'])
                except Exception as e:
                    print 'Failed to publish: ' + str(e)