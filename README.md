# Gs2Diasp

Post RSS feeds from GNU Social to Diaspora.

This program is based on [feedDiasp*](https://github.com/Debakel/feedDiasp) by [Moritz Duchêne](https://github.com/Debakel)
and [Alexey Veretennikov](https://github.com/fourier) with some additional code to replace GNU social specific formatting
with a formatting suitable for Diaspora. This includes:

* replace each group tag (```!```) with a hash tag (```#```)
* remove the leading ```<user>:``` of each message
* skip posts which are part of a conversation and start with a ```@```
* skip posts which the user marked as favorite

# Usage / Installation

* checkout this repository
* get feedDiasp: ```git submodule update --init```
* follow the install instruction of feedDiasp* which can be found under feedDiasp/README.md
* copy the settings.sample.cfg to settings.cfg and adjust the values
* run ```python publish.py```