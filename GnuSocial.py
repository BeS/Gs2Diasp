#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# GnuSocial.py: remove GNU social typical formatting from the RSS feed and replace it with Diaspora style formatting:
#               - don't start with <GNU social username>:
#               - skip posts which are part of a conversation and start with @<user>
#               - skip posts the user just favorited
#
# Copyright (C) 2015  Bjoern Schiessle <bjoern@schiessle.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class GnuSocial:

    def __init__(self, gsUserName, gsFullName):
        self.prefix = gsUserName + ': '
        self.gsUserName = gsUserName
        self.gsFullName = gsFullName

    def shouldPost(self, post):
        """ check if post starts with a '@'

        :param post: message which should be posted to Diaspora
        :type post: str
        :return: true if the message should be bosted
        :rtype: bool
        """
        if post.startswith('@'):
            return False
        if post.startswith(self.gsUserName + ' favorited something by'):
            return False
        if post.startswith(self.gsFullName + ' deleted notice'):
            return False
        return True

    def group2Hashtag(self, post):
        """ replace group operator with hash tag

        :param post: message which should be posted to Diaspora
        :type post: str
        :return: message where !foo is replaced with #bar
        :rtype: str
        """
        newPost = post.replace(' !', ' #')
        if newPost[:1] == '!':
            newPostList = list(newPost)
            newPostList[0] = '#'
            newPost = ''.join(newPostList)
        return newPost

    def cleanPost(self, post):
        """ remove leading user name from post

        :param post: message which should be posted to Diaspora
        :type post: str
        :return: message without leading <username>;
        :rtype: str
        """
        return post.replace(post[:len(self.prefix)], '')
